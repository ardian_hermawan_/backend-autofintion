<?php

use App\Http\Controllers\AutoSearchController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\KeywordController;
use App\Http\Controllers\PeraturanController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('auth')->name('auth')->group(function () {
    Route::post('/login',[UserController::class,'login'])->name('login');
    Route::post('/register',[UserController::class,'register'])->name('register');

    // Login
    Route::middleware('jwt.verify')->group(function () {
        Route::get('/logout',[UserController::class,'logout'])->name('logout');
        Route::get('/get-user',[UserController::class,'getAuthenticatedUser'])->name('get.user');
    });
});

Route::get('/peraturan/',[PeraturanController::class,'index'])->middleware('jwt.verify')->name('index');
// AUTO SEARCH
Route::prefix('auto-search')->name('auto.search.')->group(function () {
    Route::get('/filter-search',[AutoSearchController::class,'filterSearch'])->name('filter.search');
    Route::get('/filter-search-by-keyword',[AutoSearchController::class,'filterSearchByKeyword'])->name('filter.search.keyword');
    Route::get('/keyword/',[AutoSearchController::class,'getKeyword'])->name('keyword.index');
});

Route::prefix('admin')->name('admin.')->group(function () {
    // Peraturan CRUD
    Route::prefix('peraturan')->name('peraturan.')->group(function (){
        Route::get('/',[PeraturanController::class,'index'])->name('index');
        Route::get('/show/{peraturan}',[PeraturanController::class,'show'])->name('show');
        Route::post('/store',[PeraturanController::class,'store'])->name('store');
        Route::put('/update/{peraturan}',[PeraturanController::class,'update'])->name('update');
        Route::delete('/delete/{peraturan}',[PeraturanController::class,'destroy'])->name('delete');
        Route::get('/total-all-data', [PeraturanController::class,'totalAllData'])->name('totalAllData');

    });

// Kategori CRUD
    Route::prefix('kategori')->name('kategori.')->group(function (){
        Route::get('/',[KategoriController::class,'index'])->name('index');
        Route::get('/show/{kategori}',[KategoriController::class,'show'])->name('show');
        Route::post('/store',[KategoriController::class,'store'])->name('store');
        Route::put('/update/{kategori}',[KategoriController::class,'update'])->name('update');
        Route::delete('/delete/{kategori}',[KategoriController::class,'destroy'])->name('delete');
    });

// Keyword CRUD
    Route::prefix('keyword')->name('keyword.')->group(function (){
        Route::get('/',[KeywordController::class,'index'])->name('index');
        Route::get('/show/{keyword}',[KeywordController::class,'show'])->name('show');
        Route::post('/store',[KeywordController::class,'store'])->name('store');
        Route::put('/update/{keyword}',[KeywordController::class,'update'])->name('update');
        Route::delete('/delete/{keyword}',[KeywordController::class,'destroy'])->name('delete');
    });
});
