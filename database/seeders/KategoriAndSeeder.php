<?php

namespace Database\Seeders;

use App\Models\Kategori;
use App\Models\KategoriKeyword;
use App\Models\Keyword;
use Illuminate\Database\Seeder;
use League\Csv\Reader;
use League\Csv\Statement;

class KategoriAndSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csv = Reader::createFromPath(base_path(). '/database/seeders/data/SBM_2022.csv');
        $csv->setHeaderOffset(0);

        $header = $csv->getHeader(); //returns the CSV header record
        // $records = $csv->getRecords(); //returns all the CSV records as an Iterator object

        //get 25 records starting from the 11th row
        $stmt = Statement::create()
            ->offset(0)
//            ->limit(4)
        ;
        $records = $stmt->process($csv);

        foreach ($records as $record) {
            // INI BUAT SEEDER KATEGORI
            $kategori = Kategori::where('name', '=', $record['Kategori'])->first();
            if(is_null($kategori)) {
                $kategori = Kategori::create([
                    'name' => $record['Kategori']
                ]);
            }

            // Record row variable

//            $keyword1 = $record['Keyword 1'];
//            $keyword2 = $record['Keyword 2'];
//            $keyword3 = $record['Keyword 3'];
//            $keyword4 = $record['Keyword 4'];
//            $keyword5 = $record['Keyword 5'];

            $keywordCount = 21;

            // INI BUAT SEEDER KEYWORD
            for ($index = 1; $index <= $keywordCount; $index++){
                if($record['Keyword '. $index]){
                    $keyword = Keyword::where('name', '=', $record['Keyword '.$index])->first();
                    if(is_null($keyword)) {
                        $keyword = Keyword::create([
                            'name' => $record['Keyword '.$index]
                        ]);
                    }

                    $checkDuplicateKategoriKeywordExist = KategoriKeyword::where('keyword_id', $keyword->id)->where('kategori_id', $kategori->id)->first();
                    if(is_null($checkDuplicateKategoriKeywordExist)){
                        KategoriKeyword::create([
                            'kategori_id' => $kategori->id,
                            'keyword_id' => $keyword->id
                        ]);
                    }
                }
            };

            // INI BUAT SEEDER KEYWORD

//            if($keyword1 != ""){
//                $keyword = Keyword::where('name', '=', $keyword1)->first();
//                if(is_null($keyword)) {
//                    $keyword = Keyword::create([
//                        'name' => $keyword1
//                    ]);
//                }
//
//                $checkDuplicateKategoriKeywordExist = KategoriKeyword::where('keyword_id', $keyword->id)->where('kategori_id', $kategori->id)->first();
//                if(is_null($checkDuplicateKategoriKeywordExist)){
//                    KategoriKeyword::create([
//                        'kategori_id' => $kategori->id,
//                        'keyword_id' => $keyword->id
//                    ]);
//                }
//            }
//
//            if($keyword2 != ""){
//                $keyword = Keyword::where('name', '=', $keyword2)->first();
//                if(is_null($keyword)) {
//                    $keyword = Keyword::create([
//                        'name' => $keyword2
//                    ]);
//                }
//
//                $checkDuplicateKategoriKeywordExist = KategoriKeyword::where('keyword_id', $keyword->id)->where('kategori_id', $kategori->id)->first();
//                if(is_null($checkDuplicateKategoriKeywordExist)){
//                    KategoriKeyword::create([
//                        'kategori_id' => $kategori->id,
//                        'keyword_id' => $keyword->id
//                    ]);
//                }
//            }
//
//            if($keyword3 != ""){
//                $keyword = Keyword::where('name', '=', $keyword3)->first();
//                if(is_null($keyword)) {
//                    $keyword = Keyword::create([
//                        'name' => $keyword3
//                    ]);
//                }
//
//                $checkDuplicateKategoriKeywordExist = KategoriKeyword::where('keyword_id', $keyword->id)->where('kategori_id', $kategori->id)->first();
//                if(is_null($checkDuplicateKategoriKeywordExist)){
//                    KategoriKeyword::create([
//                        'kategori_id' => $kategori->id,
//                        'keyword_id' => $keyword->id
//                    ]);
//                }
//            }
//
//            if($keyword4 != ""){
//                $keyword = Keyword::where('name', '=', $keyword4)->first();
//                if(is_null($keyword)) {
//                    $keyword = Keyword::create([
//                        'name' => $keyword4
//                    ]);
//                }
//
//                $checkDuplicateKategoriKeywordExist = KategoriKeyword::where('keyword_id', $keyword->id)->where('kategori_id', $kategori->id)->first();
//                if(is_null($checkDuplicateKategoriKeywordExist)){
//                    KategoriKeyword::create([
//                        'kategori_id' => $kategori->id,
//                        'keyword_id' => $keyword->id
//                    ]);
//                }
//            }
//
//            if($keyword5 != ""){
//                $keyword = Keyword::where('name', '=', $keyword5)->first();
//                if(is_null($keyword)) {
//                    $keyword = Keyword::create([
//                        'name' => $keyword5
//                    ]);
//                }
//
//                $checkDuplicateKategoriKeywordExist = KategoriKeyword::where('keyword_id', $keyword->id)->where('kategori_id', $kategori->id)->first();
//                if(is_null($checkDuplicateKategoriKeywordExist)){
//                    KategoriKeyword::create([
//                        'kategori_id' => $kategori->id,
//                        'keyword_id' => $keyword->id
//                    ]);
//                }
//            }
        }
    }
}
