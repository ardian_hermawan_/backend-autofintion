<?php

namespace Database\Seeders;

use App\Models\Kategori;
use App\Models\Peraturan;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use League\Csv\Reader;
use League\Csv\Statement;

class PeraturanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $header = ['Uraian',' Besaran','Kategori'];
        $csv = Reader::createFromPath(base_path(). '/database/seeders/data/SBM_2022.csv');
        $csv->setHeaderOffset(0);

        $header = $csv->getHeader(); //returns the CSV header record
        // $records = $csv->getRecords(); //returns all the CSV records as an Iterator object

        //get 25 records starting from the 11th row
        $stmt = Statement::create()
            ->offset(0)
//            ->limit(4)
        ;

        $records = $stmt->process($csv);

        foreach ($records as $record) {
//            dd($record);  //do something here
            $besaranArray = explode(",", $record[" Besaran"]);
            $besaran = implode("", $besaranArray);
            Peraturan::create([
                'kategori_id' => Kategori::where('name','=',$record['Kategori'])->first()->id,
                'content'  => $record['Uraian'],
                'satuan' => $record['Satuan'],
                'harga_satuan' => $besaran,
                'tahun' => Carbon::now()->format('Y-m-d')
            ]);
        }

    }
}
