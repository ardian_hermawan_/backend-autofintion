<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Keyword extends Model
{
    use HasFactory;

    protected $table = 'keyword';

    protected $guarded = [''];

    protected $fillable = [
        'name'
    ];

    protected $appends = [
        'total_peraturan'
    ];

    public function kategori()
    {
        return $this->belongsToMany(Kategori::class);
    }

    public function getTotalPeraturanAttribute() {
        $total_peraturan = 0;

        $this->kategori()->each(function ($kategori, $key) use (&$total_peraturan) {
            $total_peraturan += count($kategori->peraturan);
        });

        return $total_peraturan;
    }
}
