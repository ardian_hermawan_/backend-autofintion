<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class KategoriKeyword extends Pivot
{
    use HasFactory;

    protected $table = 'kategori_keyword';

    protected $guarded = [''];

    protected $fillable = [
        'kategori_id',
        'keyword_id'
    ];
}
