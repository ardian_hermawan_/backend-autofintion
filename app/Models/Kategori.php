<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    use HasFactory;

    protected $table = 'kategori';

    protected $guarded = [''];

    protected $fillable = [
        'name'
    ];

    public function peraturan()
    {
        return $this->hasMany(Peraturan::class, 'kategori_id');

    }

    public function keyword()
    {
        return $this->belongsToMany(Keyword::class)->using(KategoriKeyword::class);
    }
}
