<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Peraturan extends Model
{
    use HasFactory;

    protected $table = 'peraturan';

    protected $guarded = [''];

    protected $fillable = [
        'kategori_id',
        'content',
        'satuan',
        'harga_satuan',
        'tahun'
    ];

    protected $casts = [
        'tahun' => 'date:Y-m-d'
    ];

    protected $with = [
        'kategori'
    ];

    public function kategori()
    {
        return $this->belongsTo(Kategori::class, 'kategori_id');

   }
}
