<?php

namespace App\Http\Requests\PeraturanRequest;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePeraturanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'kategori_id'       =>  'required|integer|exists:App\Models\Peraturan,id',
            'content'           =>  'required|string',
            'satuan'            =>  'required|string',
            'harga_satuan'      =>  'required|string',
            'tahun'             =>  ''
        ];
    }
}
