<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PeraturanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
//        return parent::toArray($request);
        return [
            'id'            =>  $this->id,
            'kategori_id'   => $this->kategori_id,
            'content'       =>  $this->content,
            'satuan'        =>  $this->satuan,
            'harga_satuan'  => $this->harga_satuan,
            'kategori'      => $this->kategori,
            'tahun'         =>  $this->tahun,
        ];
    }
}
