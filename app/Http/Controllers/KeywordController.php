<?php

namespace App\Http\Controllers;

use App\Http\Requests\KeywordRequest\StoreKeywordRequest;
use App\Http\Requests\KeywordRequest\UpdateKeywordRequest;
use App\Http\Resources\KeywordResource;
use App\Models\Keyword;
use Illuminate\Http\Request;

class KeywordController extends Controller
{
    public function index()
    {
        return request('search')
            ? $this->success($this->search())
            : $this->success($this->indexAllKeyword(),'Successfuly index all keyword');
    }

    public function search(){
        $keyword = Keyword::query()->where('name','like','%'.request('search').'%')->get();
        $data = request('sortBy')
            ? (request('desc'))
                ? $keyword->sortByDesc(request('sortBy'))->values()->map(function ($keyword) {
                    return new KeywordResource($keyword);
                })
                : $keyword->sortBy(request('sortBy'))->values()->map(function ($keyword){
                    return new KeywordResource($keyword);
                })
            : $keyword->map(function ($keyword){
                return new KeywordResource($keyword);
            });

        return request('per_page')
            ? $this->paginate($data, request('per_page'))
            : $data;
    }

    public function indexAllKeyword(){
        $perPage = is_null(request('per_page')) ? 10 : request('per_page');

        $keyword = request('sortBy')
            ? (request('desc'))
                ? Keyword::all()->sortByDesc(request('sortBy'))->values()->map(function ($keyword)  {
                    return new KeywordResource($keyword);
                })
                : Keyword::all()->sortBy(request('sortBy'))->values()->map(function ($keyword) {
                    return new KeywordResource($keyword);
                })
            : Keyword::all()->map(function ($keyword) {
                return new KeywordResource($keyword);
            });

        return request('per_page')
            ? $this->paginate($keyword, $perPage)
            : $keyword;
    }

    public function store(StoreKeywordRequest $request){
        $validatedData = $request->validated();
        $checkIfNameExist = Keyword::where('name','=',$request['name'])->first();

//        $this->error([],'Keyword name already exist');

        if(is_null($checkIfNameExist)){
            $keyword = Keyword::create([
                'name'      =>  $request['name'],
//                'total_search' => 0
            ]);
            $keyword->kategori()->sync($request->validated()['kategori']);

            return $this->success(new KeywordResource($keyword->fresh()), 'Successfully create ' . $keyword->name . ' keyword');
        }
        else {
            return $this->error([],'Keyword name already exist');
        }
    }

    public function show($id){
        $keyword = $this->fetchSpecificKeyword($id);
        return $this->success(new KeywordResource($keyword),'Successfully show detail keyword !');
    }

    public function update(UpdateKeywordRequest $request, $id){
        $keyword = $this->fetchSpecificKeyword($id);
//        dd($request->validated());
//        $keyword->kategori()->attach($request->);
        $keyword->name= $request->validated()['name'];
        $keyword->kategori()->sync($request->validated()['kategori']);
        $keyword->save();

//        $keyword->update($request->validated());

        return $this->success(new KeywordResource($keyword->fresh()), 'Successfully update keyword !');
    }

    public function fetchSpecificKeyword($id){
        return Keyword::with('kategori')->where('id',$id)->firstOrFail();
    }

    public function destroy(Keyword $keyword){
        return $keyword->delete()
            ? $this->success([], 'Successfully delete keyword !')
            : false;
    }
}
