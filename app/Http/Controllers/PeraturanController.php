<?php

namespace App\Http\Controllers;

use App\Http\Requests\PeraturanRequest\StorePeraturanRequest;
use App\Http\Requests\PeraturanRequest\UpdatePeraturanRequest;
use App\Http\Resources\PeraturanResource;
use App\Models\Kategori;
use App\Models\Keyword;
use App\Models\Peraturan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PeraturanController extends Controller
{
    public function totalAllData(){
        return $this->success([
            'total_peraturan'   => Peraturan::count(),
            'total_kategori'    => Kategori::count(),
            'total_keyword'     => Keyword::count()
        ]);
    }

    public function index()
    {
        return request('search')
            ? $this->success($this->search())
            : $this->success($this->indexAllPeraturan(),'Successfuly index all peraturan');
    }

    public function search(){
        $peraturan = Peraturan::query()->where('content','like','%'.request('search').'%')->get();
        $data = request('sortBy')
            ? (request('desc'))
                ? $peraturan->sortByDesc(request('sortBy'))->values()->map(function ($peraturan) {
                    return new PeraturanResource($peraturan);
                })
                : $peraturan->sortBy(request('sortBy'))->values()->map(function ($peraturan){
                    return new PeraturanResource($peraturan);
                })
            : $peraturan->map(function ($peraturan){
                return new PeraturanResource($peraturan);
            });

        return request('per_page')
            ? $this->paginate($data, request('per_page'))
            : $data;
    }

    public function indexAllPeraturan(){
        $perPage = is_null(request('per_page')) ? 10 : request('per_page');

        $peraturan = request('sortBy')
            ? (request('desc'))
                ? Peraturan::all()->sortByDesc(request('sortBy'))->values()->map(function ($peraturan)  {
                    return new PeraturanResource($peraturan);
                })
                : Peraturan::all()->sortBy(request('sortBy'))->values()->map(function ($peraturan) {
                    return new PeraturanResource($peraturan);
                })
            : Peraturan::all()->map(function ($peraturan) {
                return new PeraturanResource($peraturan);
            });

        return request('per_page')
            ? $this->paginate($peraturan, $perPage)
            : $peraturan;
    }

    public function store(StorePeraturanRequest $request){
        $validatedData = $request->validated();
        $peraturan = Peraturan::create([
            'kategori_id'   => $validatedData['kategori_id'],
            'content'   =>  $validatedData['content'],
            'satuan'    =>  $validatedData['satuan'],
            'harga_satuan'  =>  $validatedData['harga_satuan'],
            'tahun'     =>  Carbon::now()->format('Y-m-d')
        ]);

        return $this->success(new PeraturanResource($peraturan->fresh()),'Successfully create ' . $validatedData['content']. ' Peraturan !');
    }

    public function show($id){
        $peraturan = $this->fetchSpecificPeraturan($id);
        return $this->success(new PeraturanResource($peraturan),'Successfully show detail peraturan !');
    }

    public function update(UpdatePeraturanRequest $request, $id){
        $peraturan = $this->fetchSpecificPeraturan($id);
//        dd($request->validated());
        $peraturan->update($request->validated());

        return $this->success(new PeraturanResource($peraturan), 'Successfully update peraturan !');
    }

    public function fetchSpecificPeraturan($id){
        return Peraturan::where('id',$id)->firstOrFail();
    }

    public function destroy(Peraturan $peraturan){
        return $peraturan->delete()
            ? $this->success([], 'Successfully delete peraturan !')
            : false;
    }
}
