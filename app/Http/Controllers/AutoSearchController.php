<?php

namespace App\Http\Controllers;

use App\Http\Resources\KeywordResource;
use App\Models\Kategori;
use App\Models\Keyword;
use App\Models\Peraturan;
use Illuminate\Http\Request;

class AutoSearchController extends Controller
{
    public function getKeyword(){
        if(request('search')){
            $search_datas = explode(' ', request('search'));
            $keywordQ = Keyword::query();

            foreach ($search_datas as $data) {
                $keywordQ->where('name','like','%'.$data.'%');
            }
            $hasilSearch = $keywordQ->get();
        }
        else{
            $hasilSearch = Keyword::all()->map(function ($keyword) {
                $totalPeraturan = Peraturan::query()->whereHas('kategori.keyword', function ($q) use($keyword) {
                    $q->where('keyword.id', $keyword->id);
                })->count();
                $keyword->total_peraturan = $totalPeraturan;
                return new KeywordResource($keyword);
            });
        }
        return $this->success($hasilSearch,'Successfully getting keyword');
    }
    public function filterSearchByKeyword(){
        $search_datas = explode(' ', request('search'));

        // Jadi ini querynya mencari peraturan filter keyword.id yang ada di filter browser
        $peraturanQ = Peraturan::query()->whereHas('kategori.keyword', function($q) {
            $q->WhereIn('keyword.id', \request('keyword_ids'));
        });
//        dd($peraturanQ->get());

        // setelah itu dia check lagi apakah peraturan yang sudah difilter keyword tadi sesuai dengan hasil search
        foreach ($search_datas as $data) {
            $peraturanQ->where('content','like','%'.$data.'%');
        }
        $hasilSearch = $peraturanQ->get();


        // ini peraturan tidak ditemukan diatas maka kita mencarinya di kategori
        if($hasilSearch->count() == 0) {
            // mencari kategori yang mempunyai keyword yang ada di filter
            $kategoriQ = Kategori::query()->whereHas('keyword', function($q) {
                $q->WhereIn('keyword.id', \request('keyword_ids'));
            });
//            DB::enableQueryLog();

            // setelah itu dia mencari kategori yang sama dengan yang ada di search
            $kategoriQ->where(function ($q) use ($search_datas) {
                foreach ($search_datas as $data) {
                    $q->orWhere('name', 'like', '%' . $data . '%');
                }
            });

            $listKategori = $kategoriQ->get();
            $hasilSearch = [];
//            dd(DB::getQueryLog());

            // Untuk memasukkan semua peraturan yang ada pada masing masing kategori
            foreach ($listKategori as $kategori) {
                foreach ($kategori->peraturan as $peraturan) {
                    array_push($hasilSearch, $peraturan->toArray());
                }
            }
        }
        return $this->success($hasilSearch, 'success searching peraturan');
    }

    public function filterSearch(){
        $search_datas = explode(' ', request('search'));
        $peraturanQ = Peraturan::query();

        foreach ($search_datas as $data) {
            $peraturanQ->where('content','like','%'.$data.'%');
        }
        $hasilSearch = $peraturanQ->get();

        // ini untuk kategori jika peraturan tidak ditemukan
        if($hasilSearch->count() == 0){
            $kategoriQ  = Kategori::query();

            foreach ($search_datas as $data) {
                $kategoriQ->orWhere('name','like','%'.$data.'%');
            }
            $listKategori = $kategoriQ->get();
            $hasilSearch = [];

            // Untuk memasukkan semua peraturan di masing masing kategori
            foreach ($listKategori as $kategori){
                foreach ($kategori->peraturan as $peraturan){
                    array_push($hasilSearch,$peraturan->toArray());
                }
            }

            // Ini untuk keyword jika kategori tidak ditemukan
            if($listKategori->count() == 0){
                $keywordQ = Keyword::query();

                foreach ($search_datas as $data) {
                    $keywordQ->orWhere('name','like','%'.$data.'%');
                }

                $listKeyword = $keywordQ->get();

                $hasilSearch = [];

                // Untuk memasukkan semua peraturan di masing masing keyword
                foreach ($listKeyword as $keyword){
                    foreach ($keyword->kategori as $kategori){
                        foreach ($kategori->peraturan as $peraturan){
                            array_push($hasilSearch,$peraturan->toArray());
                        }
                    }
                }

            }
        }


//        dd($hasilSearch->count());

//        // filtering
//        $peraturanFilter = Keyword::with('kategori.peraturan')->where('name','like','%'.request('filter').'%')->get();
//        $listArray = [];
//        foreach ($peraturanFilter as $keyword){
//            foreach ($keyword->kategori as $kategori){
//                foreach ($kategori->peraturan as $peraturan){
//                    array_push($listArray,$peraturan->toArray());
//                }
//            }
//        }
//        dd($listArray);
        return $this->success($hasilSearch, 'succes alhamdullilah');
    }
}
