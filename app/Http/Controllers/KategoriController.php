<?php

namespace App\Http\Controllers;

use App\Http\Requests\KategoriRequest\StoreKategoriRequest;
use App\Http\Requests\KategoriRequest\UpdateKategoriRequest;
use App\Http\Resources\KategoriResource;
use App\Models\Kategori;
use App\Models\Keyword;
use Illuminate\Http\Request;

class KategoriController extends Controller
{
    public function index()
    {
        return request('search')
            ? $this->success($this->search())
            : $this->success($this->indexAllKategori(),'Successfuly index all kategori');
    }

    public function search(){
        $kategori = Kategori::query()->where('name','like','%'.request('search').'%')->get();
        $data = request('sortBy')
            ? (request('desc'))
                ? $kategori->sortByDesc(request('sortBy'))->values()->map(function ($kategori) {
                    return new KategoriResource($kategori->load('keyword'));
                })
                : $kategori->sortBy(request('sortBy'))->values()->map(function ($kategori){
                    return new KategoriResource($kategori->load('keyword'));
                })
            : $kategori->map(function ($kategori){
                return new KategoriResource($kategori->load('keyword'));
            });

        return request('per_page')
            ? $this->paginate($data, request('per_page'))
            : $data;
    }

    public function indexAllKategori(){
        $perPage = is_null(request('per_page')) ? 10 : request('per_page');

        $kategori = request('sortBy')
            ? (request('desc'))
                ? Kategori::all()->sortByDesc(request('sortBy'))->values()->map(function ($kategori)  {
                    return new KategoriResource($kategori->load('keyword'));
                })
                : Kategori::all()->sortBy(request('sortBy'))->values()->map(function ($kategori) {
                    return new KategoriResource($kategori->load('keyword'));
                })
            : Kategori::all()->map(function ($kategori) {
                return new KategoriResource($kategori->load('keyword'));
            });

        return request('per_page')
            ? $this->paginate($kategori, $perPage)
            : $kategori;
    }

    public function store(StoreKategoriRequest $request){
        $validatedData = $request->validated();

        $checkIfNameExist = Kategori::where('name','=',$request['name'])->first();

        if(is_null($checkIfNameExist)){
            $kategori = Kategori::create([
                'name'      => $validatedData['name']
            ]);
            $kategori->keyword()->sync($validatedData['keyword']);


            return $this->success(new KategoriResource($kategori->fresh()), 'Successfully create '. $kategori->name .' kategori');
        }
        else {
            return $this->error([],'Kategori name already exist');
        }
    }

    public function show($id){
        $kategori = $this->fetchSpecificKategori($id);
        return $this->success(new KategoriResource($kategori),'Successfully show detail kategori !');
    }

    public function update(UpdateKategoriRequest $request, $id){
        $kategori = $this->fetchSpecificKategori($id);
        $kategori->name = $request->validated()['name'];
        $kategori->keyword()->sync($request->validated()['keyword']);
        $kategori->save();
//        $kategori->update($request->validated());

        return $this->success(new KategoriResource($kategori->fresh()), 'Successfully update kategori !');
    }

    public function fetchSpecificKategori($id){
        return Kategori::with('keyword')->where('id',$id)->firstOrFail();
    }

    public function destroy(Kategori $kategori){
        return $kategori->delete()
            ? $this->success([], 'Successfully delete kategori !')
            : false;
    }
}
