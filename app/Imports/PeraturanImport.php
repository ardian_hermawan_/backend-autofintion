<?php

namespace App\Imports;

use App\Models\Peraturan;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\ToModel;

class PeraturanImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Peraturan([
            'name'      => $row[1],
            'satuan'    => $row[2],
            'harga_satuan' => $row[3],
            'tahun'      => Carbon::now()->format('Y-m-d')
        ]);
    }
}
